﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moveable : MonoBehaviour
{
    //speed in units per second (a tile is 1 unit)
    [SerializeField]
    private int moveSpeed = 5;
    public int MoveSpeed { get => moveSpeed; set => moveSpeed = value; }

    // Update is called once per frame
    void Update()
    {
        //should I slow down diagonal speed?

        //Movement
        this.transform.Translate(new Vector3(
            Input.GetAxis("Horizontal") * Time.deltaTime * MoveSpeed,      //translateX
            Input.GetAxis("Vertical") * Time.deltaTime * MoveSpeed,        //translateY
            0
        ), Space.World);
    }
}
